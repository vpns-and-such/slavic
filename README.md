# Slavic

A simple single client config OpenVPN server setup that is based on [Docker-OpenVPN][dockvpn].

## Prerequisites

- `sudo` permissions
- `bash`
- `git`
- Docker Engine
- Docker Compose

Here's a cheetsheet for `Ubuntu 20.04` in case you're lazy:

```bash
# Update packages list
sudo apt-get -y update

# Install deps
sudo apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# Add Docker's signing key
curl -fsSL 'https://download.docker.com/linux/ubuntu/gpg' | sudo gpg \
    --dearmor -o '/usr/share/keyrings/docker-archive-keyring.gpg'

# Add Docker's repo
echo "deb [arch=$(dpkg --print-architecture) \
    signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
    https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee \
    '/etc/apt/sources.list.d/docker.list' > /dev/null

# Install Docker
sudo apt-get -y update \
    && sudo apt-get -y install \
        docker-ce
        docker-ce-cli
        containerd.io

# Install Docker Compose
sudo curl -L \
    "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
    -o '/usr/local/bin/docker-compose' \
    && sudo chmod +x '/usr/local/bin/docker-compose'

# Grant your user perissions to execute Docker
sudo usermod -aG docker $(whoami)
```

## How2Run

- Clone this dir on your target server
- Change your workdir to this dir (do the `cd $(pwd)/slavic`)
- Run the launch script (do the `./run.sh` thing)

Here's a cheetsheet in case you're lazy:

```bash
cd "${HOME}" \
    && git clone https://gitlab.com/vpns-and-such/slavic.git \
    && cd slavic \
    && ./run.sh
```

[dockvpn]: https://github.com/dockovpn/docker-openvpn