#!/bin/bash -l
set +x

# Base settigns
CLIENT_CONFIG_PATH="$(pwd)/conf/client.ovpn"

# Simple logger
CLR='\033[0;32m'
NC='\033[0m'
log() {
    echo -e "${CLR}${1}${NC}"
}

print_conf() {
    local path
    local config
    path="${CLIENT_CONFIG_PATH}"
    config=$(cat "${path}")
    log "${config}"
}

# Start OpenVPN server
log "Launching OpenVPN server..."
if [ ! -f "${CLIENT_CONFIG_PATH}" ]; then
    log "First run detected, running prep steps..."
    echo HOST_ADDR="$(curl -s 'https://api.ipify.org')" >.env
    docker-compose up -d &&
        docker-compose exec -d dockovpn wget -O /doc/Dockovpn/client.ovpn localhost:8080
else
    docker-compose up -d
fi

# Output client config
log "Here's your client config:\n"
print_conf
